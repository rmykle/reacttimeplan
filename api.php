<?php


    $schedule = array();

    $info333String = "https://tp.uio.no/uib/timeplan/timeplan.php?id=INFO333&type=course&sort=week&sem=18v&lang=no";
    $info300String = "https://tp.uio.no/uib/timeplan/timeplan.php?id=INFO300&type=course&sort=week&sem=18v&lang=no";
    $hvlString = "https://no.timeedit.net/web/hvl/db1/pen/riq83Q067y1ZQ5Q505965XY367Zg120Y0Z6Y3QxQ57o00Z1gZgQ0qo.html";

    $info300Courses = getUIBcourse($info300String);
    $info333Courses = getUIBcourse($info333String);

    addCourseToSchedule($info300Courses, $schedule);
    addCourseToSchedule($info333Courses, $schedule);
    getHVLcourse($hvlString, $schedule);

    echo json_encode($schedule);

    function getUIBcourse($url) {
        $dom = getCourse($url);
        $coursesString = $dom->getElementById("data-js")->nodeValue;
        $courses = json_decode($coursesString);
        return $courses;

    }
    function getCourse($url){
        $data = file_get_contents($url);
        libxml_use_internal_errors(true);
        $dom = new DOMDocument();
        $dom->validateOnParse = true;

        $dom->loadHTML($data);

        return $dom;
    }

    function getHVLcourse($url, &$schedule) {
        $dom = getCourse($url);
        $content = $dom->getElementById("texttable");
        $rows = ($content->getElementsByTagName("tr"));
        
        $currentWeek = 1;

        for($i=2; $i < $rows->length; $i++){
            $row = $rows[$i];
          
 
            if($row->attributes->length > 0) {
                $class = createClass($row);
                if($class) array_push($schedule[$currentWeek][$class->day], $class);
                
            }
            else {
                $currentWeek = fixWeekNumber($row->textContent)[0];
            
                if(empty($schedule[$currentWeek])) {
                      $schedule[$currentWeek] = setupWeek();
                }

            }          
        }
    }

    function createClass($row) {
        $cells  = $row->getElementsByTagName("td");
        foreach($cells as $cell){
            //var_dump($cell);
        }

        if(strpos($cells[10]->nodeValue, "ibliote")){
            return false;
        }

        $entry = new stdClass();
        // ID
        $entry->userid = 2;

        //Day
        $entry->day = dayAsNumber($cells[1]->nodeValue);

        //Time
        $time = str_replace(" ", "", $cells[3]->nodeValue);
        $timeArray = explode("-", $time);
        $entry->startDate = $timeArray[0];
        $entry->endDate = $timeArray[1];

        //Title
        
        $entry->title = getTitle($cells[6]->nodeValue);

        $dayText = $cells[1];

        return $entry;

    }

    function getTitle($titleText) {
        if(strpos($titleText, "achelo")) {
            return "Bachelor";
        }
        elseif(strpos($titleText, "edelse")) {
            return "Ledelse";
        }
        else return "Annet";
    }

    function fixWeekNumber($string) {
        
        preg_match_all('!\d+!', $string, $matches);

    return $matches[0];
    }

    function dayAsNumber($day) {
        switch($day){
            case "Mandag":
                return 1;
            case "Tirsdag":
                return 2;
            case "Onsdag":
                return 3;
            case "Torsdag":
                return 4;
            case "Fredag":
                return 5;
            case "Lørdag":
                return 6;
            default:
                return 7;
        }
    }

    function addCourseToSchedule($courses, &$schedule) {
        foreach($courses as $course) {
            $entry = new stdClass();
            $entry->title = $course->courseid;
            $startDate = new DateTime($course->dtstart);
            $day = $startDate->format("w");

            $entry->startDate = $startDate->format("h:i");
            $endDate = new DateTime($course->dtend);
            $entry->endDate = $endDate->format("H:i");
            $entry->userid = 1;
            $week = $course->weeknr;
            
            if(empty($schedule[$week])) {
                $schedule[$week] = setupWeek();
            }

            array_push($schedule[$week][$day], $entry);
 
        }

    }

    function setupWeek() {
        $week = array();
        for($i = 1; $i <= 7; $i++){
            $week[$i] = [];
        }
        return $week;
    }
        

?>