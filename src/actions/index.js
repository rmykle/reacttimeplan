import axios from 'axios';

export const INCREMENT_WEEK = "increment_week";
export const DECREMENT_WEEK = "decrement_week";
export const CHANGE_PERSON = "change_person";
export const FETCH_HVL_COURSES = "fetch_hvl_courses";
export const FETCH_UIB_COURSES = "fetch_uib_courses";
export const FETCH_COURSES = "fetch_users";

export const incrementWeek = () => {
    return {
        type: INCREMENT_WEEK
    }
}

export const decrementWeek = () => {
    return {
        type: DECREMENT_WEEK
    }
}

export const changePerson = (person) => {

    return {
        type: CHANGE_PERSON,
        payload: person
    }
}


export const fetchCourses = () => {
    const request = axios.get("api.php");

    return (dispatch) => {
        request.then(({data}) => {
            dispatch({
                type: FETCH_COURSES, 
                payload:data
            })
        })
    }
}

