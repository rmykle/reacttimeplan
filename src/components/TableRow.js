import React from 'react';

export default (props) => {
    return (
        <tr>
           {props.first ? <th rowSpan="4">{props.time}</th> : null} 
            {addCells(props.data)}
        </tr>
    )
}

const addCells = data => {
    const cells = [];
    let id = 0;
   for(let i = 0; i < data.length; i++) {
       let cell;
       if(data[i]=== "") cell = <td key={id++}></td>
       else if(data[i].title){
       cell = <td 
       key={id++}
       rowSpan={data[i].courseLength} 
       className={data[i].userid===1 ? "rCourse" : "tCourse"}
       valign="middle"
       >{data[i].title}</td>
       }
       else cell = cell=null;
       cells.push(cell);
   }

   return cells;
}