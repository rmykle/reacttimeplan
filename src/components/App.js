import React, { Component } from 'react';
import Table from '../containers/Table';
import HeaderPanel from '../containers/HeaderPanel';
import {connect} from 'react-redux';
import {fetchCourses} from '../actions';
import _ from 'lodash';


class App extends Component {
    componentWillMount() {
        this.props.fetchCourses();
    }
    render() {
        if(_.isEmpty(this.props.courses)) return <div>Loading</div>
        
        return (
            <div>
                <HeaderPanel />
                <Table/>
            </div>
        )
    }
}

const mapStateToProps = ({courses}) => {
    return {
        courses
    }
}

export default connect(mapStateToProps, {fetchCourses})(App);