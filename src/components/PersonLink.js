import React from 'react';

export default ({person, active, onClick, id}) => {
    return (
        <li onClick={() => onClick(id)} className={active === id ? "is-active" : ""}><a>{person}</a></li>
    )
}


