import {DECREMENT_WEEK, INCREMENT_WEEK} from '../actions'


export default (state=1, action) => {

    switch(action.type) {
        case INCREMENT_WEEK: 
            return ++state;
        case DECREMENT_WEEK: 
            if(state > 1) {
                return --state;
            }
        default: 
            return state;
    }
}