import {combineReducers} from 'redux';
import SelectedWeek from './SelectedWeek';
import SelectedPerson from './SelectedPerson';
import courses from './course_reducer';


export default combineReducers({
    SelectedWeek, SelectedPerson, courses
});

