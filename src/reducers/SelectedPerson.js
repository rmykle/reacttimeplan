import { CHANGE_PERSON } from '../actions';

export default (state="0", action) => {
    switch(action.type) {
        case CHANGE_PERSON:
            return action.payload;
        default:
            return state;
    }
}