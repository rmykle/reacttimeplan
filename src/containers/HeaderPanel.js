import React from 'react';
import {connect} from 'react-redux';
import {incrementWeek, decrementWeek, changePerson} from '../actions/';
import PersonLink from '../components/PersonLink';


const HeaderPanel= (props) => {

    return (
        <div>
            <div className="hero is-link is-bold">
                <div className="hero-body">
                    <h3 className="title is-3">TIMEPLAN</h3>
                </div>
            </div>
            <div className="level">
                <div className="item-level">
                    <div className="tabs">
                        <ul>
                            <PersonLink person="Begge" active={props.SelectedPerson} id="0" onClick={props.changePerson}/>
                            <PersonLink person="Rune" active={props.SelectedPerson} id="1" onClick={props.changePerson}/>
                            <PersonLink person="Tori" active={props.SelectedPerson} id="2" onClick={props.changePerson}/>
                        </ul>
                    </div>
                </div>
                <div className="item-level">
                    <nav className="pagination is-centered">
                        <a onClick={props.decrementWeek} className="pagination-previous">Forrige</a>
                        <ul><li><a className="pagination-link is-current">{`Uke ${props.SelectedWeek}`}</a></li></ul>   
                        <a onClick={props.incrementWeek} className="pagination-next">Neste</a>
                    </nav>
                </div>
            </div>
        </div>
    )
}

const mapStateToProps = ({SelectedWeek, SelectedPerson}) => {
    return {SelectedWeek, SelectedPerson};
}

export default connect(mapStateToProps,{incrementWeek, decrementWeek, changePerson})(HeaderPanel);