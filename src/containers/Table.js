import React, { Component } from 'react';
import TableRow from '../Components/TableRow';
import {connect}from 'react-redux';

class Table extends Component {

    render() {
        console.log(this.props);
        return (
            <div className="container">
                <table className="table is-striped is-hoverable is-fullwidth">
                    <thead>
                        <tr><th></th>
                            <th className="has-text-centered" colSpan="2">Mandag</th>
                            <th className="has-text-centered" colSpan="2">Tirsdag</th>
                            <th className="has-text-centered" colSpan="2">Onsdag</th>
                            <th className="has-text-centered" colSpan="2">Torsdag</th>
                            <th className="has-text-centered" colSpan="2">Fredag</th>
                        </tr>
                    </thead>
                    <tbody>
                        {this.createRows(this.props.courses[this.props.currentWeek])}
                    </tbody>
                </table>
                </div>
        )
    }

    createRows(courses) {
        const coursesTags = this.mapCoursesToTable(courses);
        let key = 0;
        let time;
        return coursesTags.map(row => {
            time = this.getTime(time);
            return <TableRow first={key%4===0}key={key++} data={row} time={time}/>
        })
    }

    mapCoursesToTable(courses){
        const week = [];

        for(let i = 0; i < 43; i++) {
            week[i] = ["", "","","","", "","","","",""]
        }
        const courseList = this.addDayToCourses(courses);

        for(let i =0; i < courseList.length; i++) {
            const course = courseList[i];
            
            if(course.startID>0) {
                const column = ((+course.day-1)*2) + (+course.userid -1);
                week[course.startID][column] = course;
                             
                let fillerID = course.startID+1;
                for(let i = 0; i < course.courseLength; i++) {
                    if(fillerID < 42) week[fillerID++][column] = "NULL";
                }
            }
          
        }
        return week;

    }

    getTime(last) {
        if(!last) return "08:00";
        const previous = last.split(":");
        let hour = +previous[0];

        let minute = +previous[1];
        minute += 15;
        if(minute === 60) {
            minute = 0;
            hour++;
        }
        return minute === 0 ? `${hour}:00` : `${hour}:${minute}`
        
    }

    addDayToCourses(courses) {
        const courseList = [];
        for(let dayID in courses) {
            for(let courseID in courses[dayID]){
                const course = courses[dayID][courseID];
                this.formatCourse(course, dayID);
                if(course.day <6 && (course.userid==this.props.user || this.props.user == 0)) courseList.push(course);
            }
        }
 
        return courseList;
        
    }

    formatCourse(course, day) {
        course.day = day;
        course.startID = this.getStartID(course.startDate);
        course.courseLength = this.getCourseLength(course);
  
    }
    getStartID(startTime) {

        const splittedTime = startTime.split(":");
        const first = (splittedTime[0] - 8) * 4;
        const second = splittedTime[1] / 15;

        return first+second;

    }

    getCourseLength(course) {
        const endID = this.getStartID(course.endDate);
        const length = endID - course.startID;
       return length;
  
    }



}


const mapStateToProps = (state) => {
    return {
        currentWeek: state.SelectedWeek,
        courses: state.courses,
        user: state.SelectedPerson
    }
}

export default connect(mapStateToProps, null)(Table);